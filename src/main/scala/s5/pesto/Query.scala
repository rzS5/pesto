package s5.pesto

import cats._
import cats.effect.Sync
import io.circe.{Json, Encoder, Decoder}
import io.circe.generic.semiauto._
import io.circe.syntax._
import org.http4s.circe._
import org.http4s.dsl.Http4sDsl
import org.http4s.{HttpRoutes, EntityEncoder, EntityDecoder, Status, Response}

final case class Query(ingredient: String, dish: String)

object Query {
  implicit val queryEq: Eq[Query] = Eq.fromUniversalEquals
  implicit val queryDecoder: Decoder[Query] = deriveDecoder
  implicit def queryEntityDecoder[F[_]: Sync]: EntityDecoder[F, Query] = jsonOf
  implicit val queryEncoder: Encoder[Query] = deriveEncoder
  implicit def queryEntityEncoder[F[_]: Applicative]: EntityEncoder[F, Query] = jsonEncoderOf
}
