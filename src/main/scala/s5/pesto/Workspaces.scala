package s5.pesto

import cats._
import cats.implicits._
import cats.effect._
import cats.effect.implicits._
import cats.effect.concurrent._
import scala.concurrent.duration._

import io.circe.{Json, Encoder, Decoder}
import io.circe.generic.semiauto._
import io.circe.syntax._
import org.http4s.circe._
import org.http4s.{EntityEncoder}

trait Workspaces[F[_]] {
  def enumerate: F[List[Int]]
  def get(id: Int): F[Option[Workspaces.Space[F]]]
  def create(query: Query): F[(Int, Workspaces.Space[F])]
  def release(id: Int): F[Unit]
}

object Workspaces {
  final case class Answer(recipe: RecipePuppy.Recipe, ingredients: Int)
  object Answer {
    implicit val answerEncoder: Encoder[Answer] = deriveEncoder
  }

  final case class Space[F[_]](
    query: Query,
    recipes: Ref[F, List[RecipePuppy.Recipe]],
    result: Deferred[F, Option[Answer]]) {
      def snapshot(implicit C: Concurrent[F], timer: Timer[F]): F[SpaceSnapshot] =
        for {
          recipes <- this.recipes.get
          result <- this.result.get.timeoutTo(20 milliseconds, none[Answer].pure[F])
        } yield SpaceSnapshot(this.query, recipes, result)
    }

  object Space {
    def empty[F[_]: Concurrent](query: Query): F[Space[F]] =
      (Ref[F].of(List.empty[RecipePuppy.Recipe]),
        Deferred[F, Option[Answer]])
        .mapN(Space[F](query, _, _))
  }

  final case class SpaceSnapshot(
    query: Query,
    recipes: List[RecipePuppy.Recipe],
    result: Option[Answer]
  )

  object SpaceSnapshot {
    implicit val spaceSnapshotEncoder: Encoder[SpaceSnapshot] = deriveEncoder
  }

  def impl[F[_]: Concurrent]: F[Workspaces[F]] =
    (Ref[F].of(Map.empty[Int, Space[F]]), Ref[F].of(0))
      .mapN(Impl.apply[F])

  private final case class Impl[F[_]: Concurrent](spaces: Ref[F, Map[Int, Space[F]]], next: Ref[F, Int]) extends Workspaces[F] {
    final val enumerate: F[List[Int]] = spaces.get.map(_.keys.toList)

    final def get(id: Int): F[Option[Space[F]]] = spaces.get.map(_.get(id))

    final def create(query: Query): F[(Int, Space[F])] =
      next.modify(i => (i + 1, i + 1)).product(Space.empty[F](query))
        .flatMap(created => spaces.update(_ + created).as(created))

    final def release(id: Int): F[Unit] =
      spaces.update(_ - id)
  }

}
