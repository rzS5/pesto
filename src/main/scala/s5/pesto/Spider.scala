package s5.pesto

import cats._
import cats.implicits._
import cats.effect._
import org.http4s.client.Client
import org.http4s.client.middleware._
import org.http4s.Uri
import scala.concurrent.duration._

import io.chrisdavenport.log4cats.Logger

final case class Spider[F[_]: Async](R: RecipePuppy[F], C: Client[F], L: Logger[F])(implicit timer: Timer[F]) {
  def crawl(space: Workspaces.Space[F]): F[Unit] = {
    def go(page: Int): F[Int] =
      L.info(s"fetching page ${page} for ${space.query}") >>
      R.getPage(space.query.ingredient, space.query.dish, page)
        .flatMap(recipes =>
          if (recipes.isEmpty) page.pure[F]
          else
            recipes.filterA(isValid)
              .flatMap(valid =>
                space.recipes.update(_ ++ valid)) >> timer.sleep(1 seconds) >> go(page + 1))
        .recoverWith({
          case t: Throwable =>
            timer.sleep(1 seconds) >> go(page + 1)
        })

    go(0).flatTap(p => L.info(s"finished with empty page at $p")) >> complete(space)
  }

  def complete(space: Workspaces.Space[F]): F[Unit] = {
    space.recipes.get
      .map(_.sortBy(_.ingredients.size).reverse.headOption)
      .map(_.map(selected => Workspaces.Answer(selected, selected.ingredients.size)))
      .flatMap(space.result.complete)
  }

  def isValid(recipe: RecipePuppy.Recipe): F[Boolean] =
    Uri.fromString(recipe.href)
      .toOption
      .fold(false.pure[F])(
        uri =>
          FollowRedirect(10)(C).expect[String](uri)
            .as(true)
            .recover({ case _ => false }))
            .flatTap({
              case true => L.info(s"${recipe.href} is valid")
              case false => L.info(s"${recipe.href} is invalid")
            })
}
