package s5.pesto

import cats.effect._
import cats.effect.concurrent._
import cats.implicits._
import fs2._

object Main extends IOApp {
  def run(args: List[String]) = {
    Deferred[IO, ExitCode].flatMap(
      shutdown => {
        val server = PestoServer.stream[IO](shutdown.complete(ExitCode.Error).void)
          .compile
          .drain
          .as(ExitCode.Success)

        Resource.make(server.start)(_.cancel).use(
          fiber =>
            shutdown.get >> fiber.cancel.as(ExitCode.Error)
        )
    })
  }
}
