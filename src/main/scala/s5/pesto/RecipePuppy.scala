package s5.pesto

import cats._
import cats.implicits._
import cats.effect._
import cats.effect.implicits._
import io.circe.{ Encoder, Decoder, Json, HCursor }
import io.circe.generic.semiauto._
import org.http4s.{ EntityDecoder, EntityEncoder, Method, Uri, Request }
import org.http4s.client.Client
import org.http4s.client.dsl.Http4sClientDsl
import org.http4s.Method._
import org.http4s.circe._

trait RecipePuppy[F[_]] {
  def getPage(ingredient: String, dish: String, page: Int = 0): F[List[RecipePuppy.Recipe]]
  def accessible: F[Boolean]
}

object RecipePuppy {
  final case class Recipe(href: String, ingredients: List[String], title: String)
  object Recipe {
    implicit val recipeDecoder: Decoder[Recipe] = new Decoder[Recipe] {
      def apply(c: HCursor): Decoder.Result[Recipe] =
        (c.downField("href").as[String],
          c.downField("ingredients").as[String].map(processIngredientString),
          c.downField("title").as[String]).mapN(Recipe.apply)
    }

    implicit val recipeEncoder: Encoder[Recipe] = deriveEncoder

    private def processIngredientString(s: String): List[String] =
      s.split(",").toList.map(_.trim)
  }

  private final case class RecipePage(recipes: List[Recipe])
  private object RecipePage {
    implicit val recipePageDecoder: Decoder[RecipePage] = new Decoder[RecipePage] {
      def apply(c: HCursor): Decoder.Result[RecipePage] =
        c.downField("results").as[List[Recipe]]
          .map(RecipePage.apply)
    }
    implicit def recipePageEntityDecoder[F[_]: Sync]: EntityDecoder[F, RecipePage] = jsonOf
  }

  val base = Uri.uri("http://www.recipepuppy.com/api/")

  def impl[F[_]: Sync: SemigroupK](C: Client[F]): RecipePuppy[F] = new RecipePuppy[F] with Http4sClientDsl[F] {
    final override def getPage(ingredient: String, dish: String, page: Int = 0): F[List[Recipe]] = {
      val uri = base.withQueryParam("q", dish)
        .withQueryParam("i", ingredient)
        .withQueryParam("p", page)
      C.expect[RecipePage](GET(uri))
        .map(_.recipes)
    }

    final override val accessible: F[Boolean] =
      C.expect[RecipePage](GET(base))
        .as(true) <+> false.pure[F]

  }
}
