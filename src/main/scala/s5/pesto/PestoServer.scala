package s5.pesto

import cats._
import cats.effect._
import cats.implicits._
import org.http4s.server.blaze.BlazeServerBuilder
import org.http4s.client.blaze.BlazeClientBuilder
import org.http4s.implicits._
import org.http4s.server.middleware.Logger
import _root_.io.chrisdavenport.log4cats.slf4j.Slf4jLogger
import fs2._
import scala.concurrent.ExecutionContext.global
import org.http4s.client.asynchttpclient.AsyncHttpClient

object PestoServer {

  def stream[F[_]: ConcurrentEffect: SemigroupK](shutdown: F[Unit])(implicit T: Timer[F], C: ContextShift[F]): Stream[F, Nothing] = {
    for {
      logger <- Stream.eval(Slf4jLogger.create[F])
      // client <- BlazeClientBuilder[F](global).stream
      client <- AsyncHttpClient.stream[F]()
      workspaces <- Stream.eval(Workspaces.impl[F])
      puppy = RecipePuppy.impl[F](client)
      spider = Spider(puppy, client, logger)

      // Combine Service Routes into an HttpApp.
      // Can also be done via a Router if you
      // want to extract a segments not checked
      // in the underlying routes.
      httpApp = (
        PestoRoutes.queryRoutes[F](spider, workspaces) <+>
        PestoRoutes.healthcheckRoutes[F](puppy, shutdown)
      ).orNotFound

      // With Middlewares in place
      finalHttpApp = Logger.httpApp(true, true)(httpApp)

      exitCode <- BlazeServerBuilder[F]
        .bindHttp(8080, "0.0.0.0")
        .withHttpApp(finalHttpApp)
        .serve
    } yield exitCode
  }.drain
}
