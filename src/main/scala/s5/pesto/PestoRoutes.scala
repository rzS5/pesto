package s5.pesto

import cats._
import cats.data._
import cats.effect._
import cats.effect.concurrent._
import cats.effect.implicits._
import cats.implicits._
import io.circe.{Json, Encoder, Decoder}
import io.circe.generic.semiauto._
import io.circe.syntax._
import org.http4s.circe._
import org.http4s.dsl.Http4sDsl
import org.http4s.{HttpRoutes, EntityEncoder, EntityDecoder, Status, Response}

object PestoRoutes {
  final case class Result[A](result: A) extends AnyVal
  object Result {
    implicit def resultEncoder[A: Encoder]: Encoder[Result[A]] = Encoder[A].contramap[Result[A]](_.result)
    implicit def resultEntityEncoder[F[_]: Applicative, A: Encoder]: EntityEncoder[F, Result[A]] = jsonEncoderOf[F, Result[A]]
  }

  def queryRoutes[F[_]: Concurrent](S: Spider[F], W: Workspaces[F])(implicit timer: Timer[F]): HttpRoutes[F] = {
    val dsl = new Http4sDsl[F]{}
    import dsl._

    HttpRoutes.of[F] {
      case GET -> Root / "workspaces" =>
        W.enumerate.map(Result.apply)
          .map(Response(Status.Ok).withEntity[Result[List[Int]]](_))

      case req @ POST -> Root / "workspaces" =>
        req.as[Query]
          .flatMap(W.create)
          .flatMap({ case (id, space) =>
            S.crawl(space).start
              .as(Response(Status.Accepted).withEntity[Result[Int]](Result(id)))
          })

      case GET -> Root / "workspaces" / IntVar(id) =>
        W.get(id)
          .flatMap(_.traverse(_.snapshot))
          .map(_.fold[Response[F]](Response(Status.NotFound))(
            snap => Response(Status.Ok).withEntity(Result(snap))
          ))

      case DELETE -> Root / "workspaces" / IntVar(id) =>
        W.release(id).as(Response(Status.NoContent))
    }
  }

  def healthcheckRoutes[F[_]: Sync](R: RecipePuppy[F], halt: F[Unit])(implicit cs: ContextShift[F]): HttpRoutes[F] = {
    val dsl = new Http4sDsl[F]{}
    import dsl._

    HttpRoutes.of[F] {
      case GET -> Root / "healthcheck" =>
        R.accessible
          .map({
            case true => Response(Status.Ok)
            case false => Response(Status.InternalServerError)
          })

      case POST -> Root / "die" =>
        Ok() <* halt
    }
  }
}
